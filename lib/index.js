'use strict';

/**
 * Evaluates if the toString() method call using the "object" parameter
 * returns '[object Object]'.
 * 
 * @function isObject
 * @param object {*} The object ot evaluate.
 * @returns {boolean}
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

exports.guarantee = guarantee;
exports.flatten = flatten;
exports.arrays = arrays;
exports.objects = objects;
exports.NOOP = NOOP;
function isObject(object) {
    return Object.prototype.toString.call(object) === '[object Object]';
}

/**
 * Ensures that the "item" paramter matches the type of the "default_value"
 * parameter. If default_value is null or undefined, this function will throw
 * a TypeError.
 * 
 * If item is of the same type as default_value then item is returned. Otherwise,
 * default_value is returned.
 * 
 * @function guarantee
 * @param item {*} The object to evaluate.
 * @param default_value {*} The object to return if the "item" parameter is
 * not of the same type.
 * @returns {*}
 * @throws TypeError
 */
function guarantee(item, default_value) {
    if (default_value === undefined || default_value === null) {
        throw new TypeError('guarantee() passed an invalid default value');
    }

    var type = Array.isArray(default_value) === true ? 'array' : typeof default_value === 'undefined' ? 'undefined' : _typeof(default_value);
    var typeMatches = void 0;

    switch (type.toLowerCase()) {
        case 'object':
            typeMatches = isObject(item);
            break;
        case 'array':
            typeMatches = Array.isArray(item);
            break;
        default:
            typeMatches = (typeof item === 'undefined' ? 'undefined' : _typeof(item)) === type;
    }

    return typeMatches === true ? item : default_value;
}

/**
 * Iterates through the passed parameters and concatenates them into a single
 * array. Any parameters that are undefined are ignored. Any parameters that
 * are arrays, are also flatted out.
 * 
 * @function flatten
 * @param [...args] {*} The list of objects to add to the returned array.
 * @returns {array}
 */
function flatten() {
    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
    }

    var flattened = args.filter(function (item) {
        return item !== undefined;
    });

    return flattened.reduce(function (a, b) {
        a = guarantee(a, [a]);
        b = guarantee(b, [b]);

        var nonArrays = b.filter(function (item) {
            return Array.isArray(item) === false;
        });
        var embeddedArrays = b.filter(function (item) {
            return Array.isArray(item);
        });
        if (embeddedArrays.length > 0) {
            nonArrays = nonArrays.concat(flatten.apply(flatten, embeddedArrays));
        }
        return a.concat(nonArrays);
    }, []);
}

function arrays() {}

/**
 * Iterates through the passed parameters and concatenates them into a single
 * array. Any parameters that are undefined are ignored. Any parameters that
 * are arrays, are also flatted out.
 * 
 * @method flatten
 * @param [...args] {*} The list of objects to add to the returned array.
 * @returns {array}
 */
arrays.flatten = flatten.bind();

/***
 * Iterates through the 'array' parameter and returns another array where
 * any repeating objects have been removed. This method uses the
 * Array.prototype.includes method to determine equality.
 * 
 * If the 'array' parameter is not a valid array this method will through
 * a TypeError.
 * 
 * @method unique
 * @param array {Array} The array to iterate through.
 * @returns {Array}
 * @throws TypeError
 */
arrays.unique = function (array) {
    var unique = [];

    if (Array.isArray(array) === false) {
        throw new TypeError('Non-Array passed to arrays.unique()');
    }

    array.forEach(function (item, index) {
        if (index === 0) {
            unique.push(item);
            return;
        }

        if (unique.includes(item) === true) {
            return;
        }

        unique.push(item);
    });

    return unique;
};

function objects() {};

/**
 * Returns a copy of the 'object' parameter with only select properties
 * within the copy. If the 'object' parameter is not a plain object (that is,
 * if the return from the Object.prototype.toString call using the parameter
 * value is not "[object Object]") this method will throw a TypeError.
 * 
 * The remaining parameters / arguments passed to the method will be the
 * property names to copy.
 * 
 * @method cherryPick
 * @param object {Object} The plain object to copy.
 * @param [...props] {...String} The list of property names to copy.
 * @returns {Object}
 * @throws TypeError
 */
objects.cherryPick = function (object) {
    for (var _len2 = arguments.length, props = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        props[_key2 - 1] = arguments[_key2];
    }

    if (isObject(object) === false) {
        throw new TypeError('Non-PlainObject passed to object.cherryPick(...)');
    }

    var local = {};

    props.forEach(function (prop) {
        return local[prop] = object[prop];
    });

    return local;
};

/**
 * Returns a copy of the 'object' parameter with the remaining parameter values
 * used as property names to remove / delete from the copy. Uses the
 * Object.assign(...) method to create the copy of the 'object' parameter.
 * 
 * @method exclude
 * @param object {Object} The object to copy.
 * @param [props] {...String} The property names to remove from the copy.
 * @returns {Object}
 */
objects.exclude = function (object) {
    for (var _len3 = arguments.length, props = Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
        props[_key3 - 1] = arguments[_key3];
    }

    var local = Object.assign({}, object);
    var blackList = arrays.flatten(props);
    Object.keys(object).filter(function (key) {
        return blackList.includes(key);
    }).forEach(function (key) {
        return delete local[key];
    });
    return local;
};

function NOOP() {}