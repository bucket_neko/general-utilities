/* global jest describe it expect */
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _index = require('../../index');

var _givenWhenThen = require('given-when-then');

jest.unmock('../../index');
jest.unmock('given-when-then');

var uniques = [1, "bblan", 1, 3, 6, 3, 5, 2, 2, 9, 10, "bblan"];
var flattens = [[1, 2, 3], [4, 5], 6];

var uniqued = _index.arrays.unique(uniques);
var flattened = _index.arrays.flatten(flattens);

(0, _givenWhenThen.given)('arrays', (0, _givenWhenThen.then)(typeof _index.arrays === 'undefined' ? 'undefined' : _typeof(_index.arrays)).shouldBe('function'), (0, _givenWhenThen.group)('arrays.unique', (0, _givenWhenThen.then)(_typeof(_index.arrays.unique)).shouldBe('function'), (0, _givenWhenThen.when)('array ' + JSON.stringify(uniques), (0, _givenWhenThen.then)(Array.isArray(uniqued)).shouldBe(true), (0, _givenWhenThen.then)(uniqued.length).shouldBe(8)), (0, _givenWhenThen.when)('array is not an Array', (0, _givenWhenThen.then)(_index.arrays.unique).shouldThrow(TypeError, undefined))), (0, _givenWhenThen.group)('arrays.flatten', (0, _givenWhenThen.then)(_typeof(_index.arrays.unique)).shouldBe('function'), (0, _givenWhenThen.when)('array is ' + JSON.stringify(flattens), (0, _givenWhenThen.then)(Array.isArray(flattened)).shouldBe(true), (0, _givenWhenThen.then)(flattened.length).shouldBe(6))));