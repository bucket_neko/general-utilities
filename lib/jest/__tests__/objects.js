/* global jest describe it expect */
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _index = require('../../index');

jest.unmock('../../index');

describe('objects(): Container for object methods', function () {
    it('is a function', function () {
        return expect(typeof _index.objects === 'undefined' ? 'undefined' : _typeof(_index.objects)).toBe('function');
    });
    describe('objects.exclude(object, ...properties)', function () {
        it('is a method / function', function () {
            return expect(_typeof(_index.objects.exclude)).toBe('function');
        });
        describe('given an object "{one: 1, two: 2, three: 3}", objects.exclude(object,"three"):', function () {
            var object = { one: 1, two: 2, three: 3 };
            var other = _index.objects.exclude(object, 'three');
            it('returns an object with property "three" undefined', function () {
                return expect(other.three).not.toBeDefined();
            });
            it('returns an object with property "one" set to 1', function () {
                return expect(other.one).toBe(1);
            });
        });
        describe('given an object "{one: 1, two: 3, three: 3}", objects.exclude(object,["two"]):', function () {
            var object = { one: 1, two: 2, three: 3 };
            var other = _index.objects.exclude(object, ['two']);
            it('returns an object with property "two" undefined', function () {
                return expect(other.two).not.toBeDefined();
            });
            it('returns an object with property "three" set to 3', function () {
                return expect(other.three).toBe(3);
            });
        });
    });
    describe('objects.cherryPick(object, ...properties)', function () {
        it('is a function / method', function () {
            return expect(_typeof(_index.objects.cherryPick)).toBe('function');
        });
        describe('given object "{one: 1, two: 2, three: 3}"', function () {
            var object = { one: 1, two: 2, three: 3 };
            describe('when objects.cherryPick(object, "three") then', function () {
                var cpicked = _index.objects.cherryPick(object, "three");
                it('returns an object with property "three" being 3', function () {
                    return expect(cpicked.three).toBe(3);
                });
                it('returns an object with property "two" as undefined', function () {
                    return expect(cpicked.two).not.toBeDefined();
                });
            });
        });
        describe('given object is undefined', function () {
            return describe('when objects.cherryPick(undefined, "three") then', function () {
                return it('throws a TypeError', function () {
                    return expect(function () {
                        return _index.objects.cherryPick(undefined, "three");
                    }).toThrowError(TypeError);
                });
            });
        });
        describe('given object is null', function () {
            return describe('when objects.cherryPick(null, "three") then', function () {
                return it('throws a TypeError', function () {
                    return expect(function () {
                        return _index.objects.cherryPick(null, "three");
                    }).toThrowError(TypeError);
                });
            });
        });
        describe('given object is not a plain object (Object.prototype.toString.call(object) does not equal "[object Object]")', function () {
            return describe('when objects.cherryPick("blah", "three") then', function () {
                return it('throws a TypeError', function () {
                    return expect(function () {
                        return _index.objects.cherryPick("blah", "then");
                    }).toThrowError(TypeError);
                });
            });
        });
    });
});