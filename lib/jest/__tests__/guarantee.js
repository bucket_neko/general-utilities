/* global jest describe it expect */
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _index = require('../../index');

jest.unmock('../../index');

describe('guarantee(): JavaScript type ensuring', function () {
    it('is a function', function () {
        return expect(typeof _index.guarantee === 'undefined' ? 'undefined' : _typeof(_index.guarantee)).toBe('function');
    });
    describe('when passed "doe" as a default value...', function () {
        var doe = 'doe';
        it('returns "john" when passed "john" as a parametere', function () {
            return expect((0, _index.guarantee)("john", doe)).toBe("john");
        });
        it('returns "doe" when passed a number as a parameter', function () {
            return expect((0, _index.guarantee)(3, doe)).toBe(doe);
        });
        it('returns "doe" when passed undefined as a paramter', function () {
            return expect((0, _index.guarantee)(undefined, doe)).toBe(doe);
        });
    });
    describe('when passed a plain object ({}) as a default value...', function () {
        var plainObject = {};
        it('returns the plain object when passed a string as a parameter', function () {
            return expect((0, _index.guarantee)("john", plainObject)).toBe(plainObject);
        });
        it('returns the passed object when passed a plain object as a parameter', function () {
            return expect((0, _index.guarantee)(plainObject, {})).toBe(plainObject);
        });
    });
    describe('when passed undefined as a default value...', function () {
        return it('throws a TypeError', function () {
            return expect(function () {
                return (0, _index.guarantee)('john', undefined);
            }).toThrowError(TypeError);
        });
    });
    describe('when passed null as a default value', function () {
        return it('throws a Typeerror', function () {
            return expect(function () {
                return (0, _index.guarantee)('john', null);
            }).toThrowError(TypeError);
        });
    });
});