'use strict';

/**
 * Evaluates if the toString() method call using the "object" parameter
 * returns '[object Object]'.
 * 
 * @function isObject
 * @param object {*} The object ot evaluate.
 * @returns {boolean}
 */
function isObject(object) {
    return Object.prototype.toString.call(object) === '[object Object]';
}

/**
 * Ensures that the "item" paramter matches the type of the "default_value"
 * parameter. If default_value is null or undefined, this function will throw
 * a TypeError.
 * 
 * If item is of the same type as default_value then item is returned. Otherwise,
 * default_value is returned.
 * 
 * @function guarantee
 * @param item {*} The object to evaluate.
 * @param default_value {*} The object to return if the "item" parameter is
 * not of the same type.
 * @returns {*}
 * @throws TypeError
 */
export function guarantee(item, default_value) {
    if (default_value === undefined || default_value === null) {
        throw new TypeError('guarantee() passed an invalid default value');
    }
    
    let type = (Array.isArray(default_value) === true)?'array':
        typeof(default_value);
    let typeMatches;
    
    switch(type.toLowerCase()) {
        case 'object':
            typeMatches = isObject(item);
            break;
        case 'array':
            typeMatches = Array.isArray(item);
            break;
        default:
            typeMatches = (typeof(item) === type);
    }
    
    return (typeMatches === true)?item:default_value;
    
}

/**
 * Iterates through the passed parameters and concatenates them into a single
 * array. Any parameters that are undefined are ignored. Any parameters that
 * are arrays, are also flatted out.
 * 
 * @function flatten
 * @param [...args] {*} The list of objects to add to the returned array.
 * @returns {array}
 */
export function flatten(...args) {
    let flattened = args.filter( item => item !== undefined);
    
    return flattened
        .reduce((a, b) => {
            a = guarantee(a, [a]);
            b = guarantee(b, [b]);

            let nonArrays = b.filter( item => Array.isArray(item) === false);
            let embeddedArrays = b.filter( item => Array.isArray(item));
            if (embeddedArrays.length > 0) {
                nonArrays = nonArrays.concat(
                    flatten.apply(flatten, embeddedArrays)
                );
            }
            return a.concat(nonArrays);
        }, []);
}

export function arrays() {}

/**
 * Iterates through the passed parameters and concatenates them into a single
 * array. Any parameters that are undefined are ignored. Any parameters that
 * are arrays, are also flatted out.
 * 
 * @method flatten
 * @param [...args] {*} The list of objects to add to the returned array.
 * @returns {array}
 */
arrays.flatten = flatten.bind();

/***
 * Iterates through the 'array' parameter and returns another array where
 * any repeating objects have been removed. This method uses the
 * Array.prototype.includes method to determine equality.
 * 
 * If the 'array' parameter is not a valid array this method will through
 * a TypeError.
 * 
 * @method unique
 * @param array {Array} The array to iterate through.
 * @returns {Array}
 * @throws TypeError
 */
arrays.unique = (array) => {
    let unique = [];
    
    if (Array.isArray(array) === false) {
        throw new TypeError('Non-Array passed to arrays.unique()');
    }
    
    array.forEach( (item, index) => {
        if (index === 0) {
            unique.push(item);
            return;
        }
        
        if (unique.includes(item) === true) {
            return;
        }
        
        unique.push(item);
    });
    
    return unique;
};


export function objects() {};

/**
 * Returns a copy of the 'object' parameter with only select properties
 * within the copy. If the 'object' parameter is not a plain object (that is,
 * if the return from the Object.prototype.toString call using the parameter
 * value is not "[object Object]") this method will throw a TypeError.
 * 
 * The remaining parameters / arguments passed to the method will be the
 * property names to copy.
 * 
 * @method cherryPick
 * @param object {Object} The plain object to copy.
 * @param [...props] {...String} The list of property names to copy.
 * @returns {Object}
 * @throws TypeError
 */
objects.cherryPick = (object, ...props) => {
    if(isObject(object) === false) {
        throw new TypeError('Non-PlainObject passed to object.cherryPick(...)');
    }
    
    let local = {};
    
    props.forEach( prop => local[prop] = object[prop] );
    
    return local;
};

/**
 * Returns a copy of the 'object' parameter with the remaining parameter values
 * used as property names to remove / delete from the copy. Uses the
 * Object.assign(...) method to create the copy of the 'object' parameter.
 * 
 * @method exclude
 * @param object {Object} The object to copy.
 * @param [props] {...String} The property names to remove from the copy.
 * @returns {Object}
 */
objects.exclude = (object, ...props) => {
    let local = Object.assign({},object);
    let blackList = arrays.flatten(props);
    Object.keys(object)
        .filter( key => {
            return blackList.includes(key)
            })
        .forEach( key => delete local[key]);
    return local;
};

export function NOOP(){}
