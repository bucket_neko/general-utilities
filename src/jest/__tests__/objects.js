/* global jest describe it expect */
'use strict';

jest.unmock('../../index');

import {objects} from '../../index';

describe(
    'objects(): Container for object methods',
    () => {
        it(
            'is a function',
            () => expect(typeof(objects)).toBe('function')
        );
        describe(
            'objects.exclude(object, ...properties)',
            () => {
                it(
                    'is a method / function',
                    () => expect(typeof(objects.exclude)).toBe('function')
                );
                describe(
                    'given an object "{one: 1, two: 2, three: 3}", objects.exclude(object,"three"):',
                    () => {
                        let object = {one: 1, two: 2, three: 3};
                        let other = objects.exclude(object, 'three');
                        it(
                            'returns an object with property "three" undefined',
                            () => expect(other.three).not.toBeDefined()
                        );
                        it(
                            'returns an object with property "one" set to 1',
                            () => expect(other.one).toBe(1)
                        );
                    }
                );
                describe(
                    'given an object "{one: 1, two: 3, three: 3}", objects.exclude(object,["two"]):',
                    () => {
                        let object = {one: 1, two: 2, three: 3};
                        let other = objects.exclude(object, ['two']);
                        it(
                            'returns an object with property "two" undefined',
                            () => expect(other.two).not.toBeDefined()
                        );
                        it(
                            'returns an object with property "three" set to 3',
                            () => expect(other.three).toBe(3)
                        );
                    }
                );
            }
        );
        describe(
            'objects.cherryPick(object, ...properties)',
            () => {
                it(
                    'is a function / method',
                    () => expect(
                        typeof(objects.cherryPick)
                    ).toBe('function')
                );
                describe(
                    'given object "{one: 1, two: 2, three: 3}"',
                    () => {
                        let object = {one: 1, two: 2, three: 3};
                        describe(
                            'when objects.cherryPick(object, "three") then',
                            () => {
                                let cpicked = objects.cherryPick(object, "three");
                                it(
                                    'returns an object with property "three" being 3',
                                    () => expect(cpicked.three).toBe(3)
                                );
                                it(
                                    'returns an object with property "two" as undefined',
                                    () => expect(cpicked.two).not.toBeDefined()
                                );
                            }
                        );
                    }
                );
                describe(
                    'given object is undefined',
                    () => describe(
                        'when objects.cherryPick(undefined, "three") then',
                        () => 
                            it(
                                'throws a TypeError',
                                () => expect(
                                    () => objects.cherryPick(undefined, "three")
                                ).toThrowError(TypeError)
                            )
                    )
                );
                describe(
                    'given object is null',
                    () => describe(
                        'when objects.cherryPick(null, "three") then',
                        () => it(
                            'throws a TypeError',
                            () => expect(
                                () => objects.cherryPick(null, "three")
                            ).toThrowError(TypeError)
                        )
                    )
                );
                describe(
                    'given object is not a plain object (Object.prototype.toString.call(object) does not equal "[object Object]")',
                    () => describe(
                        'when objects.cherryPick("blah", "three") then',
                        () => it(
                            'throws a TypeError',
                            () => expect(
                                () => objects.cherryPick("blah","then")
                            ).toThrowError(TypeError)
                        )
                    )
                );
            }
        );
    }
);
