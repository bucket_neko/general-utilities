/* global jest describe it expect */
'use strict';

jest.unmock('../../index');
jest.unmock('given-when-then');

import {arrays} from '../../index';
import {given, when, group, thenIt, then} from 'given-when-then';

let uniques = [1, "bblan", 1, 3, 6, 3, 5, 2, 2, 9, 10, "bblan"];
let flattens = [
    [1, 2, 3],
    [4, 5],
    6
];

let uniqued = arrays.unique(uniques);
let flattened = arrays.flatten(flattens);

given('arrays',
    then(typeof(arrays)).shouldBe('function'),
    group('arrays.unique',
        then(typeof(arrays.unique)).shouldBe('function'),
        when(
            `array ${JSON.stringify(uniques)}`,
            then(Array.isArray(uniqued)).shouldBe(true),
            then(uniqued.length).shouldBe(8)
        ),            
        when(
            'array is not an Array',
            then(arrays.unique).shouldThrow(TypeError, undefined)
        )
    ),
    group(
        'arrays.flatten',
        then(typeof(arrays.unique)).shouldBe('function'),
        when(
            `array is ${JSON.stringify(flattens)}`,
            then(Array.isArray(flattened)).shouldBe(true),
            then(flattened.length).shouldBe(6)
        )
    )
);
