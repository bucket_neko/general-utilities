/* global jest describe it expect */
'use strict';

jest.unmock('../../index');

import {guarantee} from '../../index';

describe('guarantee(): JavaScript type ensuring', () => {
    it('is a function', () => expect(typeof(guarantee)).toBe('function'));
    describe(
        'when passed "doe" as a default value...',
        () => {
            let doe = 'doe';
            it(
                'returns "john" when passed "john" as a parametere',
                () => expect(guarantee("john", doe)).toBe("john")
            );
            it(
                'returns "doe" when passed a number as a parameter',
                () => expect(guarantee(3,doe)).toBe(doe)
            );
            it(
                'returns "doe" when passed undefined as a paramter', 
                () => expect(guarantee(undefined, doe)).toBe(doe)
            );
        }
    );
    describe(
        'when passed a plain object ({}) as a default value...',
        () => {
            let plainObject = {};
            it(
                'returns the plain object when passed a string as a parameter',
                () => expect(guarantee("john",plainObject)).toBe(plainObject)
            );
            it(
                'returns the passed object when passed a plain object as a parameter',
                () => expect(guarantee(plainObject, {})).toBe(plainObject)
            );
        }
    );
    describe(
        'when passed undefined as a default value...', 
        () => it(
                'throws a TypeError',
                () => expect(() => guarantee('john', undefined))
                .toThrowError(TypeError)
        )
    );
    describe(
        'when passed null as a default value',
        () => it(
            'throws a Typeerror',
            () => expect(() => guarantee('john', null))
            .toThrowError(TypeError)
        )
    );
});
